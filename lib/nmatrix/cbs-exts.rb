# Custom CBS extensions for NMatrix class
require 'nmatrix/nmatrix'

class NMatrix
  
  # Commonly used for specifying a dimension
  ROW_DIM = 0
  COL_DIM = 1
  
  attr_reader :row_names
  attr_reader :col_names
  
  class << self
    def load(input)
      input = JSON.parse(input) if input.present? && input.is_a?(String)
      matrix = N[*input, dtype: :float64]
      matrix.shape.count == 1 ? matrix.reshape([1,matrix.rows]) : matrix
    end
    
    def dump(obj)
      obj.to_json
    end
  end
        
  alias_method :internal_equals, :==
  
  def ==(matrix)
    return false unless matrix.is_a?(NMatrix)
    return false unless self.shape == matrix.shape
    self.internal_equals(matrix)
  end      
      
  def &(matrix)
    new_matrix = NMatrix.new(self.shape, false)
    self.each_with_indices.map { |val,row,col| new_matrix[row,col] = val && matrix[row,col] }
    new_matrix
  end
        
  # Set names of the matrix rows
  def row_names=(new_row_names)
    if new_row_names.present?
      raise ArgumentError.new("Row names must be supplied as an array of strings.") if not new_row_names.is_a?(Array)
      raise ArgumentError.new("Number of row names #{new_row_names.size} must equal number of rows #{self.rows}") if new_row_names.size != self.rows
      @row_names = new_row_names.dup
    end
  end
  
  def set_row_names(names)
    self.row_names = names
    self
  end
  
  # Set names of the matrix columns
  def col_names=(new_col_names)
    if new_col_names.present?
      raise ArgumentError.new("Column names must be supplied as an array of strings.") if not new_col_names.is_a?(Array)
      raise ArgumentError.new("Number of column names #{new_col_names.size} must equal number of columns #{self.cols}") if new_col_names.size != self.cols
      @col_names = new_col_names.dup
    end
  end
  
  def set_column_names(names)
    self.col_names = names
    self
  end
  
  # Calculate Moore-Penrose pseudo-inverse of the matrix:
  #  - Given matrix X, returns a matrix (Xhat) that is the same size as X.transpose
  #  - Satisfies the criteria:
  #        1.  X * Xhat * X = X, 
  #        2.  Xhat * X * Xhat = Xhat,
  #        4.  (X * Xhat) and (Xhat * X) are Hermitian
  # Uses the singular value decomposition (SVD) of the matrix, and components
  # with a value less than the tolerance (based on precision of Float) are 
  # removed. Requires LAPACKE dependency for .gesvd
  #  - Based on MATLAB pinv()
  #  - Note that LAPACKE returns V matrix as transpose of what it should be!
  #         -> X = U * S * V' (normally)
  #         -> X = U * S * V  (for LAPACKE SVD)
  def pinv(tol: self.shape.max * Float::EPSILON)
    u,s,v = self.gesvd
    rk    = (s > tol).transpose.to_a.count(true) # good singular values
    v     = v.transpose[0..-1, 0...rk]
    u     = u[0..-1, 0...rk]
    s     = s[0...rk]
    ss    = (s.transpose**-1).kron_prod(N.ones([cols,1]))
    xhat  = (v * ss).dot u.transpose
    return xhat
  end
  
  # Solves the over-determined (rows > columns) least squares problem X * B = Y
  # by using Q-R factorization to solve for the matrix B.
  def least_squares(y)  
    raise(ArgumentError, "least squares approximation only works for non-complex types") if 
      self.complex_dtype?
    
    rows, columns = self.shape
    raise(ShapeError, "system must be over-determined ( rows >= columns )") if
      rows < columns
    
    # Tolerance based on matrix size and float precision
    tol   = self.shape.max * Float::EPSILON
    
    # Perform economical Q-R factorization
    r     = self.clone  # clone because .geqrf! overwrites the data
    yy    = y.clone     # clone because .ormqr overwrites the input 
    tau   = r.geqrf!    # do the Q-R factorization
    q_t_y = r.ormqr(tau, :left, :transpose, yy) # Calculate Q'*y
    diag  = r.diagonal  # diagonal elements of R are used to determine dependency

    if diag.any? { |x| x.abs < tol }
      warn "warning: A diagonal element of R in A = QR is close to zero ;" << 
      " indicates a possible loss of precision"
    end
    
    # Indices of columns that we will keep.
    i     = (diag.abs > tol).each_index_of(true)
    
    # Transform the system X * B = Y to R1 * B = B2 where Y2 = Q' * Y
    r1    = r.row(i).col(i).upper_triangle!
    y2    = q_t_y.row(i)
    ny    = y2.shape[1]
    
    # Solve the upper triangular system
    NMatrix::BLAS::cblas_trsm(:row, :left, :upper, false, :nounit, r1.shape[0], 
                              ny, 1.0, r1, r1.shape[0], y2, ny)
                              
    bb    = N.zeros([columns,ny])
    i.each_with_index do |bb_index, y2_index|
      bb[bb_index,0..-1] = y2[y2_index,0..-1]
    end
  end
  
  # Returns a sorted list of unique values in the NMatrix
  def unique
    uniq = []
    self.each do |value|
      uniq << value if uniq.index(value).nil?
    end
    N[uniq.sort]
  end
  
  # Find the indexes (occurences) of the value 'x' in the matrix
  def each_index_of(x)
    y = if rows == 1
      self.transpose
    elsif cols == 1
      self
    elsif self.vector?
      self
    elsif 
      raise ArgumentError.new("Only works with 1D NMatrix")
    end
    
    found, current_index, x_indices = -1, -1, []
    while found
      found = y[current_index+1..-1].index(x)
      if found
        current_index += (found[ROW_DIM] + 1)
        x_indices << current_index
      end
    end
    x_indices
  end
  
  # Overriding NMatrix.hconcat an vconcat to also concatenate names, if they exist
  def hconcat(*matrices)
    new_matrix = concat(*matrices, :column).tap do |matrix|
      if (size.eql?(0) || col_names.present?) && matrices.map { |m| m.col_names.present? }.all?
        new_col_names = col_names || []
        matrices.each do |m|
          new_col_names.concat(m.col_names)
        end
        matrix.col_names = new_col_names
      end
      matrix.set_row_names(row_names) if row_names.present?
    end
  end
  
  def vconcat(*matrices)
    new_matrix = concat(*matrices, :row).tap do |matrix|
      if (size.eql?(0) || row_names.present?) && matrices.map { |m| m.row_names.present? }.all?
        new_row_names = row_names || []
        matrices.each do |m|
          new_row_names.concat(m.row_names)
        end
        matrix.row_names = new_row_names
      end
      matrix.set_column_names(col_names) if col_names.present?
    end
  end
  
  def threshold(threshold_value)
    self.each_with_indices { |val,row,col| self[row,col] = 0 if val.abs <= threshold_value }
  end

  # Frobenius (AKA Euclidean) norm is defined as the square root of the sum of 
  # the absolute squares of it's elements. 
  def frobenius_norm
    (self.dot self.transpose).trace.sqrt
  end
  
end