Gem::Specification.new do |s|
  s.name      = 'nmatrix-cbs-exts'
  s.version   = '0.1.0'
  s.date      = '2016-12-05'
  s.author    = 'Conor Wild'
  s.email     = 'conor.wild@cambridgebrainsciences.com'
  s.summary   = 'CBS extensions for NMatrix'
  s.license   = '???'
  s.homepage  = 'https://home.cambridgebrainsciences.com'

  s.add_development_dependency 'nmatrix', '~> 0.2.4'

  s.files     = ['lib/nmatrix-cbs-exts.rb', 'lib/nmatrix/cbs-exts.rb']
end
