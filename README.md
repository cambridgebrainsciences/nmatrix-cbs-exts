CBS NMatrix Extentions
===

This is the place to build extensions for NMatrix that serve the CBS platform.

Current Additions
---

1. Column and Row names for matrices.
```ruby
a = N[[1,2,3],[4,5,6]]
a.row_names = %w(row1 row2)
a.col_names = %w(varA varB varC)
```

2. Pseudo-inverse matrix function
```ruby
# Calculate Moore-Penrose pseudo-inverse of the matrix:
#  - Given matrix X, returns a matrix (Xhat) that is the same size as X.transpose
#  - Satisfies the criteria:
#        1.  X * Xhat * X = X, 
#        2.  Xhat * X * Xhat = Xhat,
#        4.  (X * Xhat) and (Xhat * X) are Hermitian
# Uses the singular value decomposition (SVD) of the matrix, and components
# with a value less than the tolerance (based on precision of Float) are 
# removed. Requires LAPACKE dependency for .gesvd
#  - Based on MATLAB pinv()
#  - Note that LAPACKE returns V matrix as transpose of what it should be!
#         -> X = U * S * V' (normally)
#         -> X = U * S * V  (for LAPACKE SVD)

a = N.rand([100,10])
a+ = N.pinv
```